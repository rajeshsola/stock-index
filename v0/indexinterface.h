#ifndef INDEXINTEFACE_H
#define INDEXINTEFACE_H

#include<iostream>
#include<string>

 class Index{
     std::string m_date;
     int m_open;
     int m_close;
     int m_highValue;
     int m_lowValue;
     int m_turnover;
     int m_volume;
     double m_PE;
     double m_PB;
     double m_dividend;
     int m_type;

     public:

     Index();

     Index(std::string,int,int,int,int,int,int,double,double,double,int);

     std::string getDate()const;

     int getOpenValue()const;

     int getCloseValue()const;

     int getHighValue()const;

     int getLowValue()const;

     int getTurnover()const;

     int getVolume()const;

     double getPriceEarningRatio()const;
     
     double getPriceBookRatio()const;

     double getDividend()const;

     int getType()const;

     virtual void display()const=0;

     virtual ~Index();

};

#endif
