#include "Indexcontainer.h"


    
IndexContainer::IndexContainer(){}

void IndexContainer::addNiftyIndex(std::string date, int open, int close, int high, int low , int volume, int turnover,
    double priceEarning, double priceBook, double dividend,int type){
        indexData.push_back(NiftyIndex(date,open,close,high,low,volume,turnover,priceEarning,priceBook
        ,dividend,type));
}

void IndexContainer::displayAll()const{
    
    std::vector<NiftyIndex>::iterator iter;
    for(auto iter=indexData.begin();iter!=indexData.end();iter++){
        iter->display();
    }
}

    double IndexContainer::findAverageOfPriceEarningRatio(int type){
        double averagePriceEarning=0.0;
        int counter=0;
        std::vector<NiftyIndex>::iterator iter;
        for(iter=indexData.begin();iter!=indexData.end();iter++){
            if(iter->getType()==type){
                averagePriceEarning=averagePriceEarning+iter->getPriceEarningRatio();
                counter=counter+1;
            }
        }
        if(counter==0){
            return -1;
        }
        return (averagePriceEarning/counter);
    }

    double IndexContainer::findMaximumDividend(int index){
        double maximumDividend=-1;
        std::vector<NiftyIndex>::iterator iter;
        for(iter=indexData.begin();iter!=indexData.end();iter++){
            if(iter->getType()==index && iter->getDividend()>maximumDividend){
                maximumDividend=iter->getDividend();
            }
        }
        return maximumDividend;
    }

    void IndexContainer::findPartsOfDate(std::string date,int *p){
        std::string dummy="";
        int counter=0;
        for(int i=0;i<date.length();i++){
            if(date[i]=='/'){
                p[counter]=(int)atoi(dummy.c_str());
                dummy="";
                counter=counter+1;
            }
            else{
                dummy=dummy+date[i];
            }

        }
        p[counter]=(int)atoi(dummy.c_str());

    }
    
    int IndexContainer::findMaximumOpenValueBetweenTwoDates(int index, std::string date1, std::string date2){
        int result=-1;
        int a[3],b[3],temp[3];
        this->findPartsOfDate(date1,a);
        this->findPartsOfDate(date2,b);
        std::vector<NiftyIndex>::iterator iter;
        for(iter=indexData.begin();iter!=indexData.end();iter++){
            if(iter->getType()==index){
                this->findPartsOfDate(iter->getDate(),temp);
                if((temp[0]>=a[0] && temp[1]>=a[1] && temp[2]>=a[2])&& (temp[0]<=b[0] && temp[1]<=b[1] && temp[2]<=b[2])){
                     if(iter->getOpenValue()>result){
                        result=iter->getOpenValue();
                    }
                }
            }
        }
        return result;
    }

    double IndexContainer::findAverageTurnOverBetweenTwoDates(int index,std::string date1,std::string date2){
        double result=-1;
        int counter=0;
        int a[3],b[3],temp[3];
        this->findPartsOfDate(date1,a);
        this->findPartsOfDate(date2,b);
        std::vector<NiftyIndex>::iterator iter;
        for(iter=indexData.begin();iter!=indexData.end();iter++){
            if(iter->getType()==index){
                this->findPartsOfDate(iter->getDate(),temp);
                if((temp[0]>=a[0] && temp[1]>=a[1] && temp[2]>=a[2])&& (temp[0]<=b[0] && temp[1]<=b[1] && temp[2]<=b[2])){
                     result=result+iter->getTurnover();
                     counter=counter+1;
                }
            }
        }
        if(counter==0){
            return -1;
        }
        return (result/counter);
    }

IndexContainer::~IndexContainer(){}

