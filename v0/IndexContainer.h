#ifndef INDEXCONTAINER_H
#define INDEXCONTAINER_H


#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include"niftyIndex.h"
#include<vector>

class IndexContainer{
    std::vector<NiftyIndex> indexData;

    public:
    IndexContainer();

    void addNiftyIndex(std::string , int , int , int, int  , int , int ,
    double , double , double,int );

    void displayAll()const;
        
    double findAverageOfPriceEarningRatio(int);

    double findMaximumDividend(int);
    
    void findPartsOfDate(std::string,int*);

    int findMaximumOpenValueBetweenTwoDates(int, std::string, std::string );

    double findAverageTurnOverBetweenTwoDates(int, std::string, std::string);

    ~IndexContainer();
    

};

#endif
